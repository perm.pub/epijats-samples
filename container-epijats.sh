#!/bin/bash

if [[ -z "$2" ]]; then
  THIS_SCRIPT=$'\u001b[1m'"$(basename $0)"$'\u001b[0m'
  echo "Usage:"
  echo "  $THIS_SCRIPT SRC_DIR DEST_DIR"
  exit 1
fi

SRC=$(realpath $1)
DEST=$(realpath $2)

podman run \
  -it --rm \
  -v $SRC:/mnt/src:Z \
  -v $DEST:/mnt/dest:Z \
  $CONTAINER_EPIJATS_ARGS \
  docker.io/castedo/epijats-samples:4 \
  python3 -m epijats --no-web-fonts /mnt/src /mnt/dest
