#!/usr/bin/python3

from epijats import Webstract, Eprint, EprinterConfig
from epijats.util import up_to_date
from epijats.jinja import WebPageGenerator, style_template_loader

import jinja2

# Python standard libraries
import os
from pathlib import Path


class SampleMaker:
    def __init__(self, generator):
        self.gen = generator
        self.dsi_base_url = "https://perm.pub"
        self.configs = dict(
            quebec=EprinterConfig(dsi_base_url=self.dsi_base_url),
            boston=EprinterConfig(dsi_base_url=self.dsi_base_url),
            lyon=EprinterConfig(dsi_base_url=self.dsi_base_url),
        )
        for name, c in self.configs.items():
            c.article_style = name

    def make_samples(self, src_dir, dest_dir):
        src = src_dir / "webstract.xml"
        webstract = Webstract.load_xml(src)
        if (src_dir / "source").exists():
            webstract.source.path = src_dir / "source"
        for style, config in self.configs.items():
            dest = dest_dir / style
            os.makedirs(dest, exist_ok=True)

            # generate example website page with embedded article HTML
            ctx = dict(
                doc=webstract.facade,
                article_style=style,
                **config.urls
            )
            self.gen.render_file("embedded.html.jinja", dest / "embedded.html", ctx)

            # make PDF file and also HTML file given to WeasyPrint
            pdf_dest = dest / "article.pdf"
            eprint = Eprint(webstract, dest, config)
            if not up_to_date(pdf_dest, src):
                eprint.make_pdf(pdf_dest)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Generate samples")
    parser.add_argument("source", type=Path, help="Path to source directory")
    parser.add_argument("output", type=Path, help="Path to ouput directory")
    args = parser.parse_args()

    gen = WebPageGenerator()
    gen.add_template_loader(jinja2.FileSystemLoader("websrc"))
    gen.add_template_loader(style_template_loader())

    maker = SampleMaker(gen)
    doc_ids = os.listdir(args.source)
    for doc_id in doc_ids:
        print(doc_id)
        maker.make_samples(args.source / doc_id, args.output / doc_id)

    ctx = dict(doc_ids=doc_ids, styles=['lyon', 'quebec', 'boston'])
    gen.render_file("index.html.jinja", args.output / "index.html", ctx)
