Epijats Samples
===============

Epijats ["webstract" files](webstracts/) generating example PDF and HTML files using
[epijats](https://gitlab.com/perm.pub/epijats).


## Dependencies

* WeasyPrint
* `pip3 install "epijats[pdf] @ git+https://gitlab.com/perm.pub/epijats.git"`
* `pip3 install git+https://gitlab.com/castedo/jsoml.git`


## To use

To generate PDF and HTML examples, run `make` in this directory.
Generated output goes to `_output` directory.
