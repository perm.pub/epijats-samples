#!/bin/bash

HERE=$(dirname "$0")
EPIJATSPATH=$(realpath "$HERE/epijats")
if [[ ! -e "$EPIJATSPATH" ]]; then
  echo "$EPIJATSPATH must exist (e.g. symlink to directory)"
  exit 1
fi

CONTAINER_EPIJATS="-e PYTHONPATH=/opt -v $EPIJATSPATH:/opt/epijats:Z" ./container-epijats.sh $@
