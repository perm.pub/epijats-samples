As of Nov 2022, the bash script `build-image` in this direction builds an OCI (Docker)
container. Castedo runs this script (on RHEL 9) to build this OCI image and then upload
the image to https://hub.docker.com/u/castedo.
Other users can then use `podman` (or `docker`) to run OCI containers using the image on
any computer that can run OCI containers (Linux is the best choice).  The operating
system inside the container is Fedora Linux.
