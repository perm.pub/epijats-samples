OUTPUT=_output

all:
	python3 make_samples.py webstracts $(OUTPUT)
	cp -rn websrc/images $(OUTPUT)/images
	@echo Done.

clean:
	rm -rf $(OUTPUT)

.PHONY : all clean
.PRECIOUS: $(OUTPUT)/.
